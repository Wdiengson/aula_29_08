var app = angular.module("agendaTelefonica",[]);
app.controller("ListaTelefonicaCtrl", function($scope){
    $scope.app ="Lista Telefônica";
    $scope.contatos = [
        {nome: "Pedro", telefone: "99999877", operadora: "Vivo", codigo:"15", apelido:"Pêpê"},
        {nome: "Ana", telefone: "7128370343", operadora: "Claro", codigo:"14", apelido:"Aninha"},
        {nome: "Maria", telefone: "107012080", operadora: "Tim", codigo:"10", apelido:"Mamá"}
    ];
});